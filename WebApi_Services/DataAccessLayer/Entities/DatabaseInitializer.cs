﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiServices.DataAccessLayer.Entities
{
    public class DatabaseInitializer
    {
        public static void Initialize(IApplicationBuilder app)
        {
            using(var serviceScope=app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
               // var context = serviceScope.ServiceProvider.GetService<AppDbContext>();
               // context.Database.EnsureCreated();
            }
        }
    }
}
