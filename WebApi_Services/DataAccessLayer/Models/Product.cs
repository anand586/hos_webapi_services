﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiServices.DataAccessLayer.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string quantity { get; set; }
        public string weight { get; set; }
        public string category { get; set; }
        public string subcategory { get; set; }
        public int storeId { get; set; }
    }
}
