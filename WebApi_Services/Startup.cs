using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccessLayer.DBEntities;
using DataAccessLayer.Repositories.Classes;
using DataAccessLayer.Repositories.Interfaces;
//using DataAccessLayer.DBEntityModels;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using WebApiServices.BusinessLogicLayer.Classes;
using WebApiServices.BusinessLogicLayer.Interfaces;
using WebApiServices.DataAccessLayer.Entities;
using WebApiServices.DataAccessLayer.Repositories.Classes;
using WebApiServices.DataAccessLayer.Repositories.Interfaces;

namespace WebApiServices
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IRegistrationService, RegistrationService>();
            services.AddScoped<IRegistrationRepo, RegistrationRepo>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IProductRepo, ProductRepo>();
            // services.AddScoped<IRegistrationRepository, RegistrationRepository>();

            services.AddControllers();
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireNonAlphanumeric = false;
            });

            //services.AddReact();
            //services.AddDbContext<AppDbContext>(options =>
            //options.UseSqlServer(
            //    Configuration.GetConnectionString("DefultConnection")));

            services.AddDbContext<ApplicationDbContext>(options =>
           options.UseSqlServer(
               Configuration.GetConnectionString("DefultConnection")));
            //services.AddScoped<AppDbContext>(options => 
            //    new AppDbContext(Configuration.GetConnectionString("DefaultConnection")));
            //services
            //.AddDefaultIdentity<IdentityUser>()
            //    .AddEntityFrameworkStores<AppDbContext>();



            services.AddCors(options =>
            {
                options.AddPolicy(name: MyAllowSpecificOrigins,
                                  builder =>
                                  {
                                      builder.WithOrigins("https://localhost:44386",
                                                          "https://localhost:44386/");
                                  });
            });


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapGet("/echo",
                //   context => context.Response.WriteAsync("echo"))
                //   .RequireCors(MyAllowSpecificOrigins);

                //endpoints.MapControllers()
                //         .RequireCors(MyAllowSpecificOrigins);

                //endpoints.MapGet("/echo2",
                //    context => context.Response.WriteAsync("echo2"));

                endpoints.MapControllers();
            });

          //  EnableCorsAttribute cors = new EnableCorsAttribute("*", "*", "*");
            app.UseCors(MyAllowSpecificOrigins);

            DatabaseInitializer.Initialize(app);
        }
    }
}
