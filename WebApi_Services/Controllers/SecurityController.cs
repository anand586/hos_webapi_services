﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApiServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SecurityController : ControllerBase
    {
        
        // GET: api/Security
        [HttpGet]
        [Route("GetUserToken")]
        public IEnumerable<string> GetUserToken()
        {
            return new string[] { "value1", "value2" };
        }

        // POST: api/Security
        [HttpPost]
        [Route("Login")]
        public void Post(string username, string password)
        {

        }

        // PUT: api/Security/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
