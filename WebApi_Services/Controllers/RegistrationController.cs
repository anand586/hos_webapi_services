﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccessLayer.DataModels;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApiServices.BusinessLogicLayer.Interfaces;

namespace WebApiServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistrationController : ControllerBase
    {

        private readonly IRegistrationService _services;

        public RegistrationController(IRegistrationService services)
        {
            _services = services;
        }

        // GET: api/Registration
       // [EnableCors("GetRegistrations")]
        [HttpGet]
        [Route("GetRegistrations")]
        public ActionResult<List<Registration>> GetRegistrations()
        {
            var getUsers = _services.getRegistrations();
            
            if(getUsers == null)
            {
                return NotFound();
            }

            return getUsers;
        }

        // POST: api/Registration
        [HttpPost]
        [Route("SaveRegistration")]
        public ActionResult<Registration> SaveRegistration([FromBody] Registration model)
        {
            var registerUsers = _services.saveRegistration(model);

            if(registerUsers == null)
            {
                return NotFound();
            }                 

            return registerUsers;
        }

        // PUT: api/Registration/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
