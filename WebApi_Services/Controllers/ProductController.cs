﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccessLayer.DataModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApiServices.BusinessLogicLayer.Interfaces;


namespace WebApiServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _service;

        public ProductController(IProductService service)
        {
            _service = service;
        }

        // GET: api/Product
        [HttpGet]
        [Route("GetProducts")]
        public List<Product> GetProducts()
        {
            var getProducts = _service.getProducts();
            return getProducts;
        }

        // POST: api/Product
        [HttpPost]
        [Route("SaveProduct")]
        public ActionResult<Product> SaveProduct([FromBody] Product model)
        {
            var products = _service.saveProduct(model);
            return products;
        }

        // PUT: api/Product/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
