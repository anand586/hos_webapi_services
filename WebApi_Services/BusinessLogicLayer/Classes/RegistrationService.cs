﻿using DataAccessLayer.DataModels;
using DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiServices.BusinessLogicLayer.Interfaces;
using WebApiServices.DataAccessLayer.Repositories.Interfaces;

namespace WebApiServices.BusinessLogicLayer.Classes
{
    public class RegistrationService : IRegistrationService
    {
        private IRegistrationRepo _repository;
        public RegistrationService(IRegistrationRepo repository)
        {
            _repository = repository;
        }

        public List<Registration> getRegistrations()
        {
            var getRegisters = _repository.getRegistrations();
            return getRegisters;
        }

        public Registration saveRegistration(Registration model)
        {
            var registerUsers = _repository.saveRegistration(model);
            return registerUsers;
        }
    }
}
