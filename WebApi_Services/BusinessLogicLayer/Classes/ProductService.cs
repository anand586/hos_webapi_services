﻿using DataAccessLayer.DataModels;
using DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiServices.BusinessLogicLayer.Interfaces;
using WebApiServices.DataAccessLayer.Repositories.Interfaces;

namespace WebApiServices.BusinessLogicLayer.Classes
{
    public class ProductService : IProductService
    {
        private readonly IProductRepo _service;

        public ProductService(IProductRepo service)
        {
            _service = service;
        }

        public List<Product> getProducts()
        {
            var products = _service.getProducts();
            return products;
        }

        public Product saveProduct(Product model)
        {
            var products = _service.saveProduct(model);
            return products;
        }
    }
}
