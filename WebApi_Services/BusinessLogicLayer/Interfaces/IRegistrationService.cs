﻿using DataAccessLayer.DataModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiServices.BusinessLogicLayer.Interfaces
{
    public interface IRegistrationService
    {
        Registration saveRegistration(Registration model);
        List<Registration> getRegistrations();
    }
}
