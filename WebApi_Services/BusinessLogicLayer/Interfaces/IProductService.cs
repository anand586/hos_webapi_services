﻿using DataAccessLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace WebApiServices.BusinessLogicLayer.Interfaces
{
    public interface IProductService
    {
        Product saveProduct(Product model);
        List<Product> getProducts();
    }
}
