﻿using DataAccessLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Repositories.Interfaces
{
   public interface IProductRepo
    {
        Product saveProduct(Product model);
        List<Product> getProducts();
    }
}
