﻿using DataAccessLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Repositories.Interfaces
{
    public interface IRegistrationRepo
    {
        Registration saveRegistration(Registration model);
        List<Registration> getRegistrations();
    }
}
