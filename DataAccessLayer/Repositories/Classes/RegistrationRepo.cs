﻿using DataAccessLayer.DataModels;
using DataAccessLayer.DBEntities;
using DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Repositories.Classes
{
    public class RegistrationRepo : IRegistrationRepo
    {
        private readonly ApplicationDbContext _context;
        public RegistrationRepo(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<Registration> getRegistrations()
        {
            var registrations = _context.Registrations;
            return registrations.ToList();
        }

        public Registration saveRegistration(Registration model)
        {
            _context.Registrations.Add(model);
            _context.SaveChanges();
            return model;
        }
    }
}
