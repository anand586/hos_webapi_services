﻿using DataAccessLayer.DataModels;
using DataAccessLayer.DBEntities;
using DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Repositories.Classes
{
    public class ProductRepo : IProductRepo
    {
        private ApplicationDbContext _context;

        public ProductRepo(ApplicationDbContext context)
        {
            _context = context;
        }
        public List<Product> getProducts()
        {
            var products = _context.Products;
            return products.ToList();
        }

        public Product saveProduct(Product model)
        {
            _context.Products.Add(model);
            _context.SaveChanges();
            return model;
        }
    }
}
